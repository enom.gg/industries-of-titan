import { createState } from 'solid-js'
import { render } from 'solid-js/web'
import './tailwind.css'

const init = () => ({
  citizens: 0,
  employees: 0,
  monitization: 96,
  salary: 40
})

const int = (str, nan = NaN) => Number.isNaN(parseInt(str, 10)) ? nan : parseInt(str, 10)

const router = {
  change (state) {
    window.location.hash = Object.keys(state).reduce((hash, key) => {
      return (state[key] === 0 ? hash : (hash && hash + ',') + key + '=' + state[key])
    }, '')
  },
  parse () {
    if (!window.location.hash) return init()
    return window.location.hash.slice(1).split(',').reduce((state, query) => {
      const [name, quantity] = query.split('=')
      return { ...state, [name]: int(quantity, 0) }
    }, init())
  }
}

function App () {
  const [state, setState] = createState(router.parse())

  let skip = false

  const earns = (convert = 0) => ((state.citizens - convert) * state.monitization) - ((state.employees + convert) * state.salary)

  const color = (convert = 0) => {
    const n = earns(convert)
    if (n > 0) return 'text-green-500'
    if (n < 0) return 'text-red-500'
  }

  const onChange = (target) => (e) => {
    setState(target, int(e.target.value, 0))
    skip = true
    router.change(state)
  }

  const onClick = (target, inc) => () => {
    setState(target, state[target] + inc)
    skip = true
    router.change(state)
  }

  window.addEventListener('hashchange', () => {
    if (!skip) setState(router.parse())
    skip = false
  })

  return (
    <div class='m-5 grid grid-cols-2'>
      <div class='m-5 grid grid-cols-2 gap-3 items-center'>
        <input
          type='number'
          value={state.monitization}
          class='border rounded px-3 py-2 bg-transparent w-full'
          onBlur={onChange('monitization')}
          onChange={onChange('monitization')}
          onKeyUp={onChange('monitization')}
        />

        <div>Monitization</div>

        <input
          type='number'
          value={state.salary}
          class='border rounded px-3 py-2 bg-transparent w-full'
          onBlur={onChange('salary')}
          onChange={onChange('salary')}
          onKeyUp={onChange('salary')}
        />

        <div>Salary</div>

        <div class='grid gap-3' style='grid-template-columns: max-content 1fr max-content'>
          <button
            class='border rounded px-3 py-2'
            onClick={onClick('citizens', -1)}
          >-</button>

          <input
            type='number'
            value={state.citizens}
            class='border rounded px-3 py-2 bg-transparent w-full'
            onBlur={onChange('citizens')}
            onChange={onChange('citizens')}
            onKeyUp={onChange('citizens')}
          />

          <button
            class='border rounded px-3 py-2'
            onClick={onClick('citizens', 1)}
          >+</button>
        </div>

        <div>Citizens</div>

        <div class='grid gap-3' style='grid-template-columns: max-content 1fr max-content'>
          <button
            class='border rounded px-3 py-2'
            onClick={onClick('employees', -1)}
          >-</button>

          <input
            type='number'
            value={state.employees}
            class='border rounded px-3 py-2 bg-transparent w-full'
            onBlur={onChange('employees')}
            onChange={onChange('employees')}
            onKeyUp={onChange('employees')}
          />

          <button
            class='border rounded px-3 py-2'
            onClick={onClick('employees', 1)}
          >+</button>
        </div>

        <div>Employees</div>

        <button
          class='border rounded px-3 py-2'
          onClick={() => setState({ citizens: state.citizens + 1, employees: state.employees - 1 })}
        >Revert 1</button>

        <button
          class='border rounded px-3 py-2'
          onClick={() => setState({ citizens: state.citizens - 1, employees: state.employees + 1 })}
        >Convert 1</button>
      </div>

      <div>
        <div class={'grid grid-cols-2 ' + color()}>
          <div>Earnings:</div>
          <div class='text-right'>{earns()}</div>
        </div>

        <div class={'grid grid-cols-2 ' + color(1)}>
          <div>Convert 1</div>
          <div class='text-right'>{earns(1)}</div>
        </div>

        <div class={'grid grid-cols-2 ' + color(2)}>
          <div>Convert 2</div>
          <div class='text-right'>{earns(2)}</div>
        </div>

        <div class={'grid grid-cols-2 ' + color(3)}>
          <div>Convert 3</div>
          <div class='text-right'>{earns(3)}</div>
        </div>

        <div class={'grid grid-cols-2 ' + color(4)}>
          <div>Convert 4</div>
          <div class='text-right'>{earns(4)}</div>
        </div>

        <div class={'grid grid-cols-2 ' + color(5)}>
          <div>Convert 5</div>
          <div class='text-right'>{earns(5)}</div>
        </div>
      </div>
    </div>
  )
}

render(() => <App />, document.getElementById('root'))
