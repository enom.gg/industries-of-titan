
// https://github.com/uuidjs/uuid/blob/master/src/regex.js
export const REGEX = /^(?:[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}|00000000-0000-0000-0000-000000000000)$/i

/**
 * Generates a UUIDv4.
 *
 * @function uuidv4#generate
 * @returns {String}
 * @see https://stackoverflow.com/a/2117523/8619916
 */
export function generate () {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
    const r = Math.random() * 16 | 0
    const v = c === 'x' ? r : (r & 0x3 | 0x8)
    return v.toString(16)
  })
}

/**
 * Tests a string to see if it's a UUIDv4.
 *
 * @function uuidv4#test
 * @param {string} uuid ID string to test
 * @returns {boolean} True if UUIDv4
 */
export function test (uuid) {
  return typeof uuid === 'string' && REGEX.test(uuid)
}
