import { createState } from 'solid-js'

import Resources from './Resources'
import Citizens from './Citizens'
import { use as router } from '--/providers/router'

/**
 * Main application component.
 *
 * @returns {JSX}
 */
export default function App () {
  const [, { match, to }] = router()

  return (
    <>
      <div class='grid grid-flow-col gap-3 mb-3'>
        <button
          class='ui.btn'
          classList={{ 'ui.selected': match('/buildings') } }
          onClick={() => to('/buildings')}
        >
          Buildings
        </button>

        <button
          class='ui.btn'
          classList={{ 'ui.selected': match('/citizens') } }
          onClick={() => to('/citizens')}
        >
          Citizens
        </button>

        <button
          class='ui.btn'
          classList={{ 'ui.selected': match('/ships') } }
          onClick={() => to('/ships')}
        >
          Ships
        </button>
      </div>

      <Switch fallback={(<p>Select one of the panels above.</p>)}>
        <Match when={match('/buildings')}>
          <Resources provider='buildings' title='Building' />
        </Match>

        <Match when={match('/citizens')}>
          <Citizens />
        </Match>

        <Match when={match('/ships')}>
          <Resources crew provider='ships' title='Ship' />
        </Match>
      </Switch>
    </>
  )
}
