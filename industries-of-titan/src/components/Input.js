import { Show } from 'solid-js/web'

import { use as resources } from '--/providers/resources'

export default function Input (props) {
  const [
    { [props.provider]: provider },
    { addDevice, removeDevice, updateDevice },
    { [props.provider]: devices }
  ] = resources()

  const hint = () => {
    const device = devices[props.name]

    const colors = {
      population: 'ui.green',
      energy: 'ui.blue',
      fuel: 'ui.yellow',
      logistics: 'text-grey-600',
      resources: 'text-grey-600'
    }

    for (const property of ['employee', 'energy', 'fuel']) {
      if (device[property] > 0) {
        return (
          <em class={'pl-2 ' + colors[property]}>
            {(property === 'fuel' ? '~' : '+') + device[property]}
            {' ' + property[0].toUpperCase() + property.slice(1)}
          </em>
        )
      }
    }
  }

  const add = (name) => {
    addDevice(props.provider, props.id, name)
  }

  const remove = (name) => {
    removeDevice(props.provider, props.id, name)
  }

  const update = (name, value) => {
    if (value instanceof Event) value = parseInt(event.target.value, 10)

    updateDevice(props.provider, props.id, name, Number.isNaN(value) ? 0 : value)
  }

  return (
    <div class='flex'>
      <div class='flex-shrink m-1'>
        <button class='ui.btn' onClick={[remove, props.name]}>-</button>
      </div>

      <div class='w-24 m-1'>
        <input
          class='ui.input'
          value={provider.store[props.id]?.devices[props.name] ?? 0}
          onKeyUp={[update, props.name]}
        />
      </div>

      <div class='flex-shrink m-1'>
        <button class='ui.btn' onClick={[add, props.name]}>+</button>
      </div>

      <div class='flex-grow self-center ml-1'>
        {props.name}

        <Show when={!props.delete}>
          {hint()}
        </Show>
      </div>

      <Show when={props.delete}>
        <div class='flex-shrink m-1'>
          <button class='ui.btn ui.red' onClick={() => update(props.name, 0)}>x</button>
        </div>
      </Show>
    </div>
  )
}
