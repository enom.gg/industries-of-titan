import { batch, createComputed, createState } from 'solid-js'
import { For } from 'solid-js/web'

import { use as resources } from '--/providers/resources'

const REQUIRE_ENGINE = 'Engine'
const REQUIRE_LIFE_SUPPORT = 'Life Support'

export default function Consumption (props) {
  const keys = {
    crew: 0,
    energy: 0,
    engine: 0,
    fuel: 0,
    life: 0,
    tiles: 0,
    waste: 0
  }

  const init = () => ({ consume: { ...keys }, delta: { ...keys} , produce: { ...keys } })

  const [{ [props.provider]: provider },, { [props.provider]: devices }] = resources()

  const [state, setState] = createState(init())

  const missing = () => Math.ceil((state.consume.crew - (state.produce.life * 3)) / 3)

  createComputed(() => {
    const computed = init()
    const sets = ['crew', 'energy', 'fuel', 'tiles', 'waste']

    Object.keys(provider.store[props.id]?.devices ?? {}).forEach((name) => {
      const quantity = provider.store[props.id].devices[name] ?? 0

      if (name === REQUIRE_ENGINE) computed.produce.engine += quantity
      if (name === REQUIRE_LIFE_SUPPORT) computed.produce.life += quantity

      sets.forEach((key) => {
        const value = devices[name][key] ?? 0

        computed.consume[key] -= quantity * (value < 0 ? value : 0)
        computed.delta[key] += quantity * value
        computed.produce[key] += quantity * (value > 0 ? value : 0)
      })
    })

    setState(computed)
  })

  const color = (key, fallback) => state.delta[key] < 0 ? 'ui.red' : fallback

  const check = (key) => state.delta[key] === 0
    ? (<strong>{String.fromCharCode(10003)}</strong>)
    : (state.delta[key] > 0
      ? (key === 'fuel' ? '~' : '+') + state.delta[key]
      : state.delta[key])

  return (
    <>
      <div>
        <div class='grid grid-cols-2 items-center'>
          <h2 class=''>Yields</h2>

          <dl class='ui.toc'>
            <dt>Tiles</dt>
            <dd>{state.consume.tiles}</dd>
          </dl>
        </div>

        <dl
          class='text-lg content-center ui.toc'
          style='grid-template-columns: 1fr max-content max-content'
        >
          <Show when={props.crew}>
            <dt class='ui.green'>Crew</dt>
            <dd class='ui.green'></dd>
            <dd class='ui.green'>{state.delta.crew}</dd>
          </Show>

          <dt class={color('energy', 'ui.blue')}>Energy</dt>
          <dd class={'text-sm leading-7 ' + color('energy', 'ui.blue')}>
            ({state.produce.energy} - {state.consume.energy})
          </dd>
          <dd class={color('energy', 'ui.blue')}>{check('energy')} </dd>

          <dt class={color('fuel', 'ui.yellow')}>Fuel</dt>
          <dd class={'text-sm leading-7 ' + color('fuel', 'ui.yellow')}>
            ({state.produce.fuel} - {state.consume.fuel})
          </dd>
          <dd class={color('fuel', 'ui.yellow')}>{check('fuel')}</dd>
        </dl>
      </div>

      <Show when={props.crew && state.produce.engine < 1}>
        <div class='ui.red font-bold text-center'>Missing Engine</div>
      </Show>

      <Show when={props.crew && missing() > 0}>
        <div class='ui.red font-bold text-center'>
          Missing {missing()} Life Support device{missing() > 1 ? 's' : ''}
        </div>
      </Show>
    </>
  )
}
