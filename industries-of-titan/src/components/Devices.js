import { createState } from 'solid-js'
import { For, Show } from 'solid-js/web'

import Input from './Input'
import { use as resources } from '--/providers/resources'

export default function Devices (props) {
  const [{ [props.provider]: provider },, { [props.provider]: devices }] = resources()

  const filter = (keyword) => {
    return Object.keys(devices).filter((name) => {
      const device = parseInt(provider.store[props.id]?.devices[name] ?? 0, 10)

      return props.filter && device === 0 ? false : devices[name].keywords.includes(keyword)
    })
  }

  const keywords = () => props.provider === 'buildings'
    ? ['population', 'energy', 'fuel', 'logistics', 'resources']
    : ['crew', 'energy', 'fuel', 'defense', 'offense']

  return (
    <For each={keywords()}>
      {(keyword) => (
        <Show when={!props.filter || filter(keyword).length}>
          <h2 class='mb-1 mt-2'>{keyword[0].toUpperCase() + keyword.slice(1)}</h2>

          <For each={filter(keyword)}>
            {(name) => (
              <Input delete={props.filter} id={props.id} name={name} provider={props.provider} />
            )}
          </For>
        </Show>
      )}
    </For>
  )
}
