import { createComputed, createState } from 'solid-js'

import Consumption from './Consumption'
import Devices from './Devices'
import { use as resources } from '--/providers/resources'
import { use as router } from '--/providers/router'

/**
 * Component that manages resource costs.
 *
 * @returns {JSX}
 */
export default function Resources (props) {
  const url = '/' + props.provider
  const capture = '{id}'

  const [
    { [props.provider]: provider },
    { createStore, destroyStore, restoreStore, updateStore  }
  ] = resources()

  const [routed, { match, to }] = router()

  const init = () => {
    const view = match(url + '/' + capture)?.id

    if (view && !provider.store[view]) {
      to(url, true)
    } else {
      return { view }
    }
  }

  const [state, setState] = createState(init())

  const create = () => {
    to(url + '/' + createStore(props.provider, props.title).id)
  }

  const remove = (id) => {
    destroyStore(props.provider, id)
    to(url, true)
  }

  const rename = (id, event) => {
    updateStore(props.provider, id, 'name', event.target.value)
  }

  const trashed = () => {
    return Object.keys(provider.trash).some((id) => provider.trash[id])
  }

  const view = (id) => {
    to(url + '/' + id)
  }

  createComputed(() => {
    const view = match(url + '/' + capture)?.id

    if (view && !provider.store[view]) {
      to(url, true)
    } else {
      setState('view', view)
    }
  })

  return (
    <>
      <h1 class='m-2 mt-0'>{props.title}s</h1>

      <div
        class='grid gap-3 items-start overflow-hidden'
        style='grid-template-columns: max-content 1fr;'
      >
        <div class='grid grid-flow-row gap-3'>
          <button onClick={create} class='ui.btn ui.blue'>Create {props.title}</button>

          <For each={Object.values(provider.store)}>
            {(data) => (
              <button
                class='ui.btn'
                classList={{ 'ui.selected': data.id === state.view }}
                onClick={[view, data.id]}
              >
                {data.name}
              </button>
            )}
          </For>

          <Show when={trashed()}>
            <button
              class='ui.btn ui.blue'
              onClick={[restoreStore, props.provider]}
            >
              Undo Destroy
            </button>
          </Show>
        </div>

        <Show when={state.view}>
          <div
            class='grid gap-3 overflow-hidden h-full'
            style='grid-template-rows: max-content max-content 1fr;'
          >
            <div
              class='grid items-center gap-3 pl-3 pr-3'
              style='grid-template-columns: max-content 1fr max-content;'
            >
              <label for='input-name'>Name</label>

              <input
                id='input-name'
                class='ui.input'
                value={provider.store[state.view]?.name}
                onKeyUp={[rename, state.view]}
              />

              <button
                class='ui.btn ui.red'
                onClick={[remove, state.view]}
              >
                Destroy
              </button>
            </div>

            <Consumption crew={props.crew} id={state.view} provider={props.provider} />

            <div class='grid grid-cols-2 gap-3 h-full overflow-hidden'>
              <div class='overflow-y-scroll h-full'>
                <Devices id={state.view} provider={props.provider} />
              </div>

              <div class='overflow-y-scroll h-full'>
                <Devices filter id={state.view} provider={props.provider} />
              </div>
            </div>
          </div>
        </Show>
      </div>
    </>
  )
}
