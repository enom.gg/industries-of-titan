import { createState } from 'solid-js'

export const LOCAL_STORAGE_KEY = 'industries-of-titan/citizens'

/**
 * Citizens component that displays to cost of converting citizens to employees.
 *
 * @returns {JSX}
 */
export default function Citizens () {
  const init = () => {
    try {
      const local = window.localStorage.getItem(LOCAL_STORAGE_KEY)

      if (local) return JSON.parse(local)
    } catch (e) {
      // Fall through to default return
    }

    return { citizens: 0, employees: 0, monitization: 96, salary: 40 }
  }

  const [state, setState] = createState(init())

  const earns = (employees = 0) => {
    const income = (state.citizens - employees) * state.monitization
    const wages = (state.employees + employees) * state.salary

    return income - wages
  }

  const color = (employees = 0) => {
    const n = earns(employees)

    if (n > 0) return 'text-green-500'

    if (n < 0) return 'text-red-500'
  }

  const persist = () => {
    try {
      window.localStorage.setItem(LOCAL_STORAGE_KEY, JSON.stringify(state))
    } catch (e) {
      console.error('Citizens~persist could not save to window.localStorage');
      console.log(e);
    }
  }

  const convert = (employee) => {
    setState('employees', state.employees + employee)
    setState('citizens', state.citizens - employee)
    persist()
  }

  const onChange = (target) => (e) => {
    const value = parseInt(e.target.value, 10)

    setState(target, Number.isNaN(value) ? 0 : value)
    persist()
  }

  const onClick = (target, inc) => () => {
    setState(target, state[target] + inc)
    persist()
  }

  return (
    <>
      <h1 class='m-2 mt-0'>Citizens</h1>

      <div class='grid grid-cols-2'>
        <div class='grid grid-cols-2 gap-3 content-start items-center'>
          <input
            type='number'
            value={state.monitization}
            class='ui.input'
            onBlur={onChange('monitization')}
            onChange={onChange('monitization')}
            onKeyUp={onChange('monitization')}
          />

          <div>Monitization</div>

          <input
            type='number'
            value={state.salary}
            class='ui.input'
            onBlur={onChange('salary')}
            onChange={onChange('salary')}
            onKeyUp={onChange('salary')}
          />

          <div>Salary</div>

          <div class='grid gap-3' style='grid-template-columns: max-content 1fr max-content'>
            <button
              class='ui.btn'
              onClick={onClick('citizens', -1)}
            >-</button>

            <input
              type='number'
              value={state.citizens}
              class='ui.input'
              onBlur={onChange('citizens')}
              onChange={onChange('citizens')}
              onKeyUp={onChange('citizens')}
            />

            <button
              class='ui.btn'
              onClick={onClick('citizens', 1)}
            >+</button>
          </div>

          <div>Citizens</div>

          <div class='grid gap-3' style='grid-template-columns: max-content 1fr max-content'>
            <button
              class='ui.btn'
              onClick={onClick('employees', -1)}
            >-</button>

            <input
              type='number'
              value={state.employees}
              class='ui.input'
              onBlur={onChange('employees')}
              onChange={onChange('employees')}
              onKeyUp={onChange('employees')}
            />

            <button
              class='ui.btn'
              onClick={onClick('employees', 1)}
            >+</button>
          </div>

          <div>Employees</div>

          <button
            class='ui.btn'
            onClick={[convert, -1]}
          >Revert 1</button>

          <button
            class='ui.btn'
            onClick={[convert, 1]}
          >Convert 1</button>
        </div>

        <div>
          <div class={'grid grid-cols-2 ' + color()}>
            <div>Earnings:</div>
            <div class='text-right'>{earns()}</div>
          </div>

          <div class={'grid grid-cols-2 ' + color(1)}>
            <div>Convert 1</div>
            <div class='text-right'>{earns(1)}</div>
          </div>

          <div class={'grid grid-cols-2 ' + color(2)}>
            <div>Convert 2</div>
            <div class='text-right'>{earns(2)}</div>
          </div>

          <div class={'grid grid-cols-2 ' + color(3)}>
            <div>Convert 3</div>
            <div class='text-right'>{earns(3)}</div>
          </div>

          <div class={'grid grid-cols-2 ' + color(4)}>
            <div>Convert 4</div>
            <div class='text-right'>{earns(4)}</div>
          </div>

          <div class={'grid grid-cols-2 ' + color(5)}>
            <div>Convert 5</div>
            <div class='text-right'>{earns(5)}</div>
          </div>
        </div>
      </div>
    </>
  )
}
