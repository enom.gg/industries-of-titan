import { createContext, createState, useContext } from 'solid-js'

const RouterContext = createContext()
const paramRegex = /\{([^/}]+)}/g

export function RouterProvider (props) {
  const parse = () => {
    if (!window.location.hash) return '/'
    return window.location.hash.slice(1)
  }

  const [state, setState] = createState({ route: parse() })

  const actions = {
    parse,
    is (route) {
      return state.route === route
    },
    match (route) {
      if (route === '/') return {}

      const params = {}
      const current = state.route.slice(1).split('/')
      const parts = route.slice(1).split('/')
      const length = current.length > parts.length ? current.length : parts.length

      for (let i = 0; i < length; i++) {
        if (current[i] === undefined) return
        if (parts[i] === undefined) return params

        if (parts[i][0] === '{' && parts[i].slice(-1) === '}') {
          params[parts[i].slice(1, -1)] = current[i]
        } else if (parts[i] !== current[i]) {
          return
        }
      }

      return params
    },
    to (route, replace = false) {
      if (replace) {
        window.location.replace('#' + route)
      } else {
        window.location.hash = route
      }
    }
  }

  window.addEventListener('hashchange', () => {
    setState('route', parse())
  })

  return (
    <RouterContext.Provider value={[state, actions]}>
      {props.children}
    </RouterContext.Provider>
  )
}

export function use () {
  return useContext(RouterContext)
}
