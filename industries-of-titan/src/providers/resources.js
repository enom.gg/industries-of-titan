import { batch, createContext, createState, useContext } from 'solid-js'

import buildings from './resources/buildings.json'
import ships from './resources/ships.json'
import { generate as uuidv4 } from '--/utils/uuidv4'

const ResourcesContext = createContext()
let i = 1

export const LOCAL_STORAGE_KEY = 'industries-of-titan/resources'

export function ResourcesProvider (props) {
  const objectify = (array) => array.reduce((results, data) => {
    return { ...results, [data.id]: data }
  }, {})

  const init = (provider) => {
    try {
      const local = window.localStorage.getItem(LOCAL_STORAGE_KEY)

      if (local) {
        const json = JSON.parse(local)
        const buildings = objectify(json.buildings)
        const ships = objectify(json.ships)

        return {
          buildings: { store: buildings, trash: {} },
          ships: { store: ships, trash: {} }
        }
      }
    } catch (e) {
      // Fall through to default init value
    }

    return {
      buildings: { store: {}, trash: {} },
      ships: { store: {}, trash: {} }
    }
  }

  const [state, setState] = createState(init())

  const updateDevice = (provider, id, name, quantity, force = false) => {
    const existing = state[provider].store[id].devices[name] ?? 0
    const total = force ? quantity : existing + quantity

    if (total === 0) {
      setState(provider, 'store', id, 'devices', name, undefined)
    } else {
      setState(provider, 'store', id, 'devices', name, total)
    }
  }

  const persist = () => {
    try {
      const buildings = Object.values(state.buildings.store)
      const ships = Object.values(state.ships.store)

      window.localStorage.setItem(LOCAL_STORAGE_KEY, JSON.stringify({ buildings, ships }))
    } catch (e) {
      console.error('providers/resources~persist could not save to window.localStorage');
      console.log(e);
    }
  }

  const actions = {
    createStore (provider, title) {
      const name = title + ' ' + (i++)
      const id = uuidv4()

      setState(provider, 'store', id, { id, name, devices: {} })
      persist()

      return state[provider].store[id]
    },
    addDevice (provider, id, name) {
      updateDevice(provider, id, name, 1)
      persist()
    },
    destroyStore (provider, id) {
      batch(() => {
        setState(provider, 'trash', id, state[provider].store[id])
        setState(provider, 'store', id, undefined)
      })
      persist()
    },
    removeDevice (provider, id, name) {
      updateDevice(provider, id, name, -1)
      persist()
    },
    restoreStore (provider) {
      if (!Object.values(state[provider].trash).length) return
      const last = Object.values(state[provider].trash).slice(-1)[0]

      batch(() => {
        setState(provider, 'store', last.id, last)
        setState(provider, 'trash', last.id, undefined)
      })
      persist()
    },
    updateStore (provider, id, property, value) {
      setState(provider, 'store', id, property, value)
      persist()
    },
    updateDevice (provider, id, name, value) {
      updateDevice(provider, id, name, value, true)
      persist()
    }
  }

  const defininitions = { buildings, ships }

  return (
    <ResourcesContext.Provider value={[state, actions, defininitions]}>
      {props.children}
    </ResourcesContext.Provider>
  )
}

export function use () {
  return useContext(ResourcesContext)
}
