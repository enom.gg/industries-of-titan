import { ResourcesProvider } from './resources'
import { RouterProvider } from './router'

/**
 * Main data provider.
 *
 * @returns {JSX}
 */
export default function Provider (props) {
  return (
    <RouterProvider>
      <ResourcesProvider>
        {props.children}
      </ResourcesProvider>
    </RouterProvider>
  )
}
