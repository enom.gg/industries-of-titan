import { DevicesProvider } from './devices'
import { RouterProvider } from './router'

export default function Provider (props) {
  return (
    <RouterProvider>
      <DevicesProvider>
        {props.children}
      </DevicesProvider>
    </RouterProvider>
  )
}
