import { createContext, createState, useContext } from 'solid-js'

import devices from './devices.json'
import { use as router } from './router'

const DevicesContext = createContext()

export function DevicesProvider (props) {
  const [, { parse, change }] = router()

  const [gets, sets] = createState({ selected: parse() })

  let skip = false

  const commit = (name, quantity, force = false) => {
    const index = gets.selected.findIndex((device) => device.name === name)

    if (index === -1) {
      if (quantity) sets('selected', (data) => [...data, { name, quantity }])
    } else {
      sets('selected', index, 'quantity', (value) => force ? quantity : value + quantity)

      if (!gets.selected[index].quantity) {
        sets('selected', (data) => [...data.slice(0, index), ...data.slice(index + 1)])
      }
    }

    skip = true

    change(gets.selected)
  }

  const state = { devices, state: gets }

  const actions = {
    add (name) {
      commit(name, 1)
    },
    remove (name) {
      commit(name, -1)
    },
    update (name, event) {
      const value = event instanceof Event ? parseInt(event.target.value, 10) : event

      commit(name, Number.isNaN(value) ? 0 : value, true)
    }
  }

  window.addEventListener('hashchange', () => {
    if (!skip) sets('selected', parse())

    skip = false
  }, false)

  return (
    <DevicesContext.Provider value={[state, actions]}>
      {props.children}
    </DevicesContext.Provider>
  )
}

export function use () {
  return useContext(DevicesContext)
}
