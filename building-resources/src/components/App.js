import Consumption from './Consumption'
import Devices from './Devices'
import Search from './Search'

/**
 * Main application component.
 *
 * @returns {JSX}
 */
export default function App () {
  return (
    <div class='grid grid-cols-2 gap-3 p-2 h-full overflow-hidden'>
      <div class='border-r border-gray-600 h-full pr-3 overflow-y-scroll'>
        <Search />
      </div>

      <div class='h-full overflow-y-scroll'>
        <Consumption />

        <hr />

        <Devices />
      </div>
    </div>
  )
}
