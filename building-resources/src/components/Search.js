import { createState } from 'solid-js'
import { For } from 'solid-js/web'

import Input from './Input'
import { use as data } from '--/data/devices'

export default function Search (props) {
  const [{ devices }] = data()

  const [gets, sets] = createState({ term: '' })

  const regex = () => new RegExp(gets.term, 'i')

  const keyworded = (name, keyword) => devices[name].keywords?.includes(keyword)

  const keywords = (name) => devices[name].keywords ?? []

  const search = (keyword) => Object.keys(devices).filter((name) => {
    const words = keywords(name)

    if (!words.includes(keyword)) return false

    if (gets.term === '') return true

    const match = regex()

    if (match.test(name)) return true

    return words.some((word) => match.test(word))
  })

  const hint = (name, keyword) => {
    if (!keyworded(name, keyword)) return

    const device = devices[name]

    const colors = {
      population: 'ui.green',
      energy: 'ui.blue',
      fuel: 'ui.orange',
      logistics: 'text-grey-600',
      resources: 'text-grey-600'
    }

    for (const property of ['employee', 'energy', 'fuel']) {
      if (device[property] > 0) {
        return (
          <em class={'pl-2 ' + colors[keyword]}>
            +{device[property]} {property[0].toUpperCase() + property.slice(1)}
          </em>
        )
      }
    }
  }

  return (
    <>
      <div class='flex'>
        <div class='flex-shrink m-1'>
          <button class='ui.input' onClick={() => sets('term', '')}>x</button>
        </div>

        <div class='flex-grow m-1'>
          <input
            class='ui.input w-full text-left'
            value={gets.term}
            onKeyUp={(event) => sets('term', event.target.value)}
           />
         </div>
      </div>

      <For each={['population', 'energy', 'fuel', 'logistics', 'resources']}>
        {(keyword) => (
          <>
            <hr />

            <h2>{keyword[0].toUpperCase() + keyword.slice(1)}</h2>

            <For each={search(keyword)}>
              {(name) => (<Input name={name} hint={hint(name, keyword)} />)}
            </For>
          </>
        )}
      </For>
    </>
  )
}
