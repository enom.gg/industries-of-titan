import { createComputed, createState } from 'solid-js'
import { For } from 'solid-js/web'

import { use as data } from '--/data/devices'

const map = { employee: 0, energy: 0, fuel: 0 }

export default function Consumption (props) {
  const [{ devices, state }] = data()

  const [gets, sets] = createState({ ...map, tiles: 0 })

  createComputed(() => {
    const balance = { ...map }
    const consume = { ...map }
    const produce = { ...map }
    let tiles = 0

    state.selected.forEach((selected) => {
      const device = devices[selected.name]
      const employee = selected.quantity * (device.employee ?? 0)
      const energy = selected.quantity * (device.energy ?? 0)
      const fuel = selected.quantity * (device.fuel ?? 0)

      tiles += device.tiles ?? 0

      balance.employee += employee
      balance.energy += energy
      balance.fuel += fuel

      consume.employee -= employee < 0 ? employee : 0
      consume.energy -= energy < 0 ? energy : 0
      consume.fuel -= fuel < 0 ? fuel : 0

      produce.employee += employee > 0 ? employee : 0
      produce.energy += energy > 0 ? energy : 0
      produce.fuel += fuel > 0 ? fuel : 0
    })

    sets('balance', balance)
    sets('consume', consume)
    sets('produce', produce)
    sets('tiles', tiles)
  })

  const color = (type, fallback) => gets.balance[type] < 0 ? 'ui.red' : fallback

  const check = (value) => value === 0
    ? (<strong>{String.fromCharCode(10003)}</strong>)
    : (value > 0 ? '+' + value : value)

  return (
    <>
      <div class='grid grid-cols-2 gap-3'>
        <For each={['produce', 'consume']}>
          {(metric) => (
            <div>
              <h3>{metric[0].toUpperCase() + metric.slice(1)}</h3>

              <dl>
                <dt class='ui.green'>Employees</dt>
                <dd class='ui.green'>{gets[metric].employee}</dd>

                <dt class='ui.blue'>Energy</dt>
                <dd class='ui.blue'>{gets[metric].energy}</dd>

                <dt class='ui.orange'>Fuel</dt>
                <dd class='ui.orange'>{gets[metric].fuel}</dd>
              </dl>
            </div>
          )}
        </For>
      </div>

      <hr />

      <div class='grid grid-cols-2 gap-3'>
        <h2>Balance</h2>

        <h3>Tiles <span class='float-right pr-5 text-base'>{gets.tiles}</span></h3>

        <dl class='text-lg ui.border-items ui.pad-items col-span-2'>
          <dt class='ui.green'>Employees</dt>
          <dd class='ui.green'>{gets.balance.employee}</dd>

          <dt class={color('energy', 'ui.blue')}>Energy</dt>
          <dd class={color('energy', 'ui.blue')}>{check(gets.balance.energy)}</dd>

          <dt class={color('fuel', 'ui.orange')}>Fuel</dt>
          <dd class={color('fuel', 'ui.orange')}>{check(gets.balance.fuel)}</dd>
        </dl>
      </div>
    </>
  )
}
