import { For, Show } from 'solid-js/web'

import Input from './Input'
import { use as data } from '--/data/devices'

export default function Devices () {
  const [{ devices, state }] = data()

  const keyworded = (keyword) => state.selected.filter((selected) => {
    return devices[selected.name].keywords.includes(keyword)
  })

  return (
    <For each={['population', 'energy', 'fuel', 'logistics', 'resources']}>
      {(keyword) => (
        <Show when={keyworded(keyword).length}>
          <h4>{keyword[0].toUpperCase() + keyword.slice(1)}</h4>

          <For each={keyworded(keyword)}>
            {(device) => (<Input name={device.name} delete />)}
          </For>
        </Show>
      )}
    </For>
  )
}
