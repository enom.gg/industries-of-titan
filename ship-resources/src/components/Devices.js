import { createComputed, createState } from 'solid-js'
import { For } from 'solid-js/web'

import Input from './Input'
import { use as data } from '--/data/devices'

export default function Devices (props) {
  const [gets, sets] = createState({ engine: false, life: false })

  const [{ devices, state }, { add, remove, update }] = data()

  const value = (device) => {
    return state.selected.find((selected) => selected.name === device)?.quantity ?? 0
  }

  createComputed(() => {
    let crew = 0
    let engine = false
    let life = 0

    state.selected.forEach((selected) => {
      const crewed = devices[selected.name].crew ?? 0

      crew += selected.quantity * (crewed > 0 ? crewed : 0)
      life += selected.quantity * (selected.name === 'Life Support' ? 3 : 0)

      if (selected.name === 'Engine') engine = true
    })

    const missing = Math.ceil((crew - life) / 3)

    sets('engine', !engine)
    sets('life', life < crew ? missing : false)
  })

  const type = (keyword) => state.selected.filter((selected) => {
    return devices[selected.name].keywords.includes(keyword)
  })

  return (
    <>
      <Show when={gets.engine}>
        <div class='ui.red font-bold m-5'>Missing Engine</div>
      </Show>

      <Show when={gets.life}>
        <div class='ui.red font-bold m-5'>Missing {gets.life} Life Support device{gets.life > 1 ? 's' : ''}</div>
      </Show>

      <For each={['crew', 'energy', 'fuel', 'defense', 'offense']}>
        {(keyword) => (
          <Show when={type(keyword).length}>
            <h4>{keyword[0].toUpperCase() + keyword.slice(1)}</h4>

            <For each={type(keyword)}>
              {(device) => (<Input name={device.name} delete />)}
            </For>
          </Show>
        )}
      </For>
    </>
  )
}
