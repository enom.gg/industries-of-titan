import { Show } from 'solid-js/web'

import { use as data } from '--/data/devices'

export default function Input (props) {
  const [{ state }, { add, remove, update }] = data()

  const value = (device) => {
    return state.selected.find((selected) => selected.name === device)?.quantity ?? 0
  }

  return (
    <div class='flex'>
      <div class='flex-shrink m-1'>
        <button class='ui.input' onClick={[remove, props.name]}>-</button>
      </div>

      <div class='w-24 m-1'>
        <input
          class='ui.input'
          value={value(props.name)}
          onKeyUp={[update, props.name]}
        />
      </div>

      <div class='flex-shrink m-1'>
        <button class='ui.input' onClick={[add, props.name]}>+</button>
      </div>

      <div class='flex-grow self-center ml-1'>
        {props.name}

        <Show when={props.hint}>
          <em class={'pl-2 ' + props.hint.color}>{props.hint.text}</em>
        </Show>
      </div>

      <Show when={props.delete}>
        <div class='flex-shrink m-1'>
          <button class='ui.input' onClick={() => update(props.name, 0)}>x</button>
        </div>
      </Show>
    </div>
  )
}
