import { createState } from 'solid-js'
import { For } from 'solid-js/web'

import Input from './Input'
import { use as data } from '--/data/devices'

export default function Search (props) {
  const [{ devices, state }, { add, remove, update }] = data()

  const [gets, sets] = createState({ term: '' })

  const regex = () => new RegExp(gets.term, 'i')

  const keyworded = (name, keyword) => devices[name].keywords?.includes(keyword)
  const keywords = (name) => devices[name].keywords ?? []

  const search = (keyword) => Object.keys(devices).filter((name) => {
    const words = keywords(name)

    if (!words.includes(keyword)) return false

    if (gets.term === '') return true

    const match = regex()

    if (match.test(name)) return true

    return words.some((word) => match.test(word))
  })

  const hint = (name, keyword, color) => {
    if (!keyworded(name, keyword)) return

  }

  return (
    <>
      <div class='flex'>
        <div class='flex-shrink m-1'>
          <button class='ui.input' onClick={() => sets('term', '')}>x</button>
        </div>

        <div class='flex-grow m-1'>
          <input
            class='ui.input w-full text-left'
            value={gets.term}
            onKeyUp={(event) => sets('term', event.target.value)}
           />
         </div>
      </div>

      <For each={['crew', 'energy', 'fuel', 'defense', 'offense']}>
        {(keyword) => (
          <>
            <hr />

            <h2>{keyword[0].toUpperCase() + keyword.slice(1)}</h2>

            <For each={search(keyword)} fallback={(<div class='text-center'>-</div>)}>
              {(name) => (<Input name={name} hint={hint(name, keyword, 'ui.green')} />)}
            </For>
          </>
        )}
      </For>
    </>
  )
}
