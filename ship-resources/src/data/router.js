import { createContext, useContext } from 'solid-js'

const RouterContext = createContext()

export function RouterProvider (props) {
  const state = {}

  const actions = {
    change (payload) {
      window.location.hash = payload.reduce((hash, item) => {
        return (hash && hash + ',') +
          encodeURI(item.name.replaceAll(' ', '+')) +
          '=' + item.quantity
      }, '')
    },
    parse () {
      if (!window.location.hash) return []
      return window.location.hash.slice(1).split(',').map((item) => {
        const [name, quantity] = item.split('=')
        return { name: decodeURI(name.replaceAll('+', ' ')), quantity: parseInt(quantity, 10) }
      })
    }
  }

  return (
    <RouterContext.Provider value={[state, actions]}>
      {props.children}
    </RouterContext.Provider>
  )
}

export function use () {
  return useContext(RouterContext)
}
